*Docker image с инфраструктурными инструментами для использования в пайплайнах*
В образе есть:

    TERRAFORM_VERSION=1.3.6
    KUBECTL_VERSION=v1.26.0
    HELM_VERSION=v3.10.3
    ANSIBLE_VERSION=6.6.0-r0
    VAULT_VERSION=1.15.4
    YC_VERSION=0.116.0
